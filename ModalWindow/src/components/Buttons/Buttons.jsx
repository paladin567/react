import { Component } from "react";
import '../Buttons/Buttons.css'
class Buttons extends Component {
   handleClick = (e) => {
       this.props.onClick(e)
   }
    render() {
        const {backgroundColor, text, closeStyle, dataKey, onClick} = this.props
        return (
            <>
            <button className={closeStyle ? ("close-btn") : "btn"} 
                    style={{backgroundColor}}
                    data-key={dataKey} 
                    onClick={onClick && ((e)=>this.handleClick(e))}>
                {text}
            </button>
            </>
        )
    }
}

export default Buttons