import { Component } from "react";
import '../Modal/Modal.css'
import Buttons from "../Buttons/Buttons";
import {modalWindowDeclarations} from './ModalContent.js'

const getModalContent = (id) => {
    return modalWindowDeclarations.find( modal => modal.modalID === id);  
}

class Modal extends Component {
    constructor(props){
        super()
        const modalContent = getModalContent(props.openModalID)
        this.state = {
            modalContent: modalContent
        }
    }
    handleClose = () => {
        this.props.onClose()
    }
    render() {
        const {headerText, bodyText, closeButton, actions} = this.state.modalContent
        return (
            <>
            <div className="backdrop" onClick={()=>this.handleClose()}></div>
            <div className="modal">
                    <div className="modal-header">
                        {headerText}
                        {closeButton && (<Buttons onClick={()=>this.handleClose()} closeStyle={true}/>)}
                    </div>
                    <div className="modal-body">
                        {bodyText}
                    </div>
                    <div className="modal-actions">
                        <>
                        {actions.map(button => {
                            const {id, backgroundColor, text} = button;
                            return <Buttons key={id} backgroundColor={backgroundColor} text={text}/>
                        })}
                        </>
                    </div>
                </div>
            </>
        )
    }
}
export default Modal