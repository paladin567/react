
const modalWindowDeclarations = [
    {
        modalID: 1,
        headerText: "First modal test",
        closeButton: true,
        bodyText: "The Modal Text / HTML Link Element is another simple element, that allows you to add a text or html link to trigger a modal dialog, in conjunction with the Modal Element. The two work hand in hand. With this simple element, you can add some text to trigger a modal, or you can add html or shortcodes (like an image) to trigger a modal that way as well.",
        actions: [{id:1, backgroundColor:"#007bff", text:"OK"},
                {id:2, backgroundColor:"#7c7c7c", text:"Cancel"}]
    },
    {
        modalID: 2,
        headerText: "Second modal test",
        closeButton: false,
        bodyText: "Much principle twenty agreed wrote direct kept hastened answer china. Produce painful ashamed sentiments theirs fifteen connection all boisterous learn. Might raptures drift wicket pretty hearted built girl saw hopes tall satisfied village begin wrong ought latter. Adapted period event yourself the misery provision speaking procuring limits shy conveying concealed excellence. Regular tore chicken apartments sir.",
        actions: [{id:1, backgroundColor: "#d1c000", text: "Save"},
                {id:2, backgroundColor:"#6a1d1e", text:"Cancel"}]
    }
  ]
  export {modalWindowDeclarations}
  