import {Component} from 'react';
import Buttons from './components/Buttons/Buttons';
import Modal from './components/Modal/Modal';
import './App.css';

class App extends Component {
  constructor() {
    super()
    this.state = {
      visible: false,
      isOpenedModalID: null,
    } 
  }
  toggle = () => {
    this.setState({
      visible: !this.state.visible
    })
  }
  getData = (e) => {
    const dataKey = parseInt(e.currentTarget.attributes['data-key'].value);
    this.setState({
      isOpenedModalID: dataKey
    })
  }
  render() {
    return (
      <>
        <Buttons backgroundColor="#007bff" text="Open first modal" dataKey={1} onClick={(e) => {
          this.toggle()
          this.getData(e)
          }}/>
        <Buttons backgroundColor="#28a745" text="Open second modal" dataKey={2} onClick={(e) => {
          this.toggle()
          this.getData(e)
          }}/>
        {this.state.visible && (<Modal
        openModalID={this.state.isOpenedModalID}
        onClose={this.toggle}
        />)}
      </>
    )
  }
}
export default App;
