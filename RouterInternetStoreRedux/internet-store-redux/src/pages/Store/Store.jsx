import "../../components/ProductCard/ProductCard.css";
import Buttons from "../../components/Button/Buttons";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { showModalDelete } from "../../store/Modal/modal-actions";


const Store = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);

  const modalOpen = (card) => {
    dispatch(showModalDelete({card: card}));
  };
  
  return (
    <>
      {!cart.length ? (<p className="no-items">No items in the cart</p>) : 
      cart.map((product) => {
        const { title, price, image, color, vendor, qty } = product;
        return (
          <div key={vendor} className="card">
            <div className="delete-from-cart">
              <Buttons onClick={() => modalOpen(product)} closeStyle={true} />
            </div>
            <div className="image-container">
              <img src={image} alt="" />
            </div>
            <p className="card-title">{title}</p>
            <div className="card-description">
              <span className="price">{price} UAH</span>
              <span className="color">{color}</span>
            </div>
            <div className="quantity">
              <span>Quantity</span>
              <input type="text" placeholder={qty} disabled />
            </div>
          </div>
        );
      })}
      
    </>
  );
};

export default Store;
