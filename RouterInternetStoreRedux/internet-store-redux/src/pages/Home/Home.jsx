import { useSelector } from "react-redux";
import ProductsList from "../../components/ProductsList/ProductsList";

const Home = () => {
  const products = useSelector((state) => state.products.loadedProducts);
  return <ProductsList products={products} />;
};
Home.defaultProps = {
  products: [],
};
export default Home;
