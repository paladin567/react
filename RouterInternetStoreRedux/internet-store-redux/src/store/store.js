import {legacy_createStore as createStore, applyMiddleware, compose} from 'redux'
import rootReducer from './rootReducer'
import thunk from 'redux-thunk';

const getDataLS = (category) => {
    const lScategory = localStorage.getItem(category);
    if (!lScategory) return [];
    try {
      const value = JSON.parse(lScategory);
      return value;
    } catch (e) {
      return [];
    }
  };

const INITIAL_STATE = {
    products: {},
    cart: getDataLS('cart'),
    favorites: getDataLS('favorite'),
    modal: []
}

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f => f)

const syncLS = function(store) {
    return function(next) {
        return function(action) {
            if(action.type === 'ADD_FAVORITE') {
                store.getState()
                const result = next(action)
                localStorage.setItem("favorite", JSON.stringify(store.getState().favorites));
                store.getState()
                return result
            }
            if(action.type === 'ADD_TO_CART' || action.type === 'REMOVE_FROM_CART' ) {
                store.getState()
                const result = next(action)
                localStorage.setItem("cart", JSON.stringify(store.getState().cart));
                store.getState()
                return result
            }
            return next(action)
        }
    }
}

const store = createStore(rootReducer, INITIAL_STATE, compose(applyMiddleware(thunk, syncLS), devTools)
)

export default store