import { combineReducers } from "redux";
import favoritesReducer from './Favorites/favorites-reducer'
import cartReducer from './Cart/cart-reducer'
import productsReducer from "./Products/products-reducer";
import modalReducer from "./Modal/modal-reducer";

const rootReducer = combineReducers({
    products: productsReducer,
    favorites: favoritesReducer,
    cart: cartReducer,
    modal: modalReducer
})

export default rootReducer