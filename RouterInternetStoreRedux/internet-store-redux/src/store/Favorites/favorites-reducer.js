

const favoritesReducer = (state = [], action) => {
    switch (action.type) {
		case 'ADD_FAVORITE': {
			const isExist = state.some((card)=> card.vendor === action.payload.card.vendor)
			if (isExist) return state.filter((card)=> card.vendor !== action.payload.card.vendor);
			return [...state, action.payload.card]
		}
		default: {
			return state
		}
	}
}

export default favoritesReducer