export const addFavorite = card => {
    return {
        type: 'ADD_FAVORITE',
        payload: { card }
    }
}

export const removeFavorite = id => {
    return {
        type: 'REMOVE_FAVORITE',
        payload: { id }
    }
}