
const modalReducer = (state = [], action) => {
    switch (action.type) {
		case 'SHOW_MODAL_CART': {
			return {...state, isOpen: true, renderModal: 'cart', card: action.payload.options.card}
		}
		case 'SHOW_MODAL_DELETE': {
			return {...state, isOpen: true, renderModal: 'delete', card: action.payload.options.card}
		}
		case 'HIDE_MODAL': {
			return {...state, isOpen: false, renderModal: null, card: null}
		}
		default: {
			return state
		}
	}
}

export default modalReducer