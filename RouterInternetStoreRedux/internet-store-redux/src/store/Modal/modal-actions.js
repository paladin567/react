export const showModalCart = (options) => {
    return {
        type: 'SHOW_MODAL_CART',
        payload: {options}
    }
}

export const showModalDelete = (options) => {
    return {
        type: 'SHOW_MODAL_DELETE',
        payload: {options}
    }
}

export const hideModal = () => {
    return {
        type: 'HIDE_MODAL',   
    }
}