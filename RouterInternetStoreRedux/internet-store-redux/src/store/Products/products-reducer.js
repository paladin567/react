

const productsReducer = (state = {}, action) => {
    switch (action.type) {
        case 'GET_PRODUCTS_REQUESTED': {
            return {...state, isLoading: true}
        }
        case 'GET_PRODUCTS_SUCCESS': {
            return {...state, isLoading: false, hasError: false, loadedProducts: [...action.payload.response]}
        }
        case 'GET_PRODUCTS_ERROR': {
            return {...state, isLoading: false, hasError: true}
        }
        default: {
            return state
        }
    }
}

export default productsReducer