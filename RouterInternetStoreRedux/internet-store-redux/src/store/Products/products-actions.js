import getProducts from "../../api/getProducts"

export const fetchProducts = () => async dispatch => {
    dispatch(loadedRequest())
    try {
        const data = await getProducts()
        dispatch(loadedSuccess(data)) 
    } catch (e) {
        dispatch(loadedWithError())
    }
     
}
export const loadedRequest = () => {
    return {
        type: "GET_PRODUCTS_REQUESTED"
    }
}
export const loadedSuccess = (response) => {
    return {
        type: "GET_PRODUCTS_SUCCESS",
        payload: { response }
    }
}

export const loadedWithError = () => {
    return {
        type: "GET_PRODUCTS_ERROR"
    }
}
