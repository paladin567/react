export const addToCart = card => {
    return {
        type: 'ADD_TO_CART',
        payload: { card }
    }
}

export const removeFromCart = vendor => {
    return {
        type: 'REMOVE_FROM_CART',
        payload: { vendor }
    }
}