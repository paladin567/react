
const cartReducer = (state = [], action) => {
    switch (action.type) {
		case 'ADD_TO_CART': {
			const isExist = state.some((card)=> card.vendor === action.payload.card.vendor);
            if (isExist) return state.map(item => item.vendor === action.payload.card.vendor ? {...item, qty: item.qty + 1} : item)
            return [...state, {...action.payload.card, qty: 1}] 
		}
		case 'REMOVE_FROM_CART': {
			return state.filter((card)=> card.vendor !== action.payload.vendor)
		}
		default: {
			return state
		}
	}
}

export default cartReducer