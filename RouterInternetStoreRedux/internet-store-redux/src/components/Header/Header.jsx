import "../Header/Header.css";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux"

const Header = () => {
  const favorites = useSelector((state) => state.favorites)
  const cart = useSelector((state) => state.cart)
  
  return (
    <>
      <header className="header">
        <div className="logo-container">
          <img src="./logo-store.png" alt="" />
        </div>
        <div className="nav">
          <Link className="navLink" to="/">
            <div className="nav-home">
              <img className="home" src="./nav-home.svg" alt="" />
              <p>Home Page</p>
            </div>
          </Link>
          <Link className="navLink" to="/favorites">
            <div className="nav-favorites">
              <img className="star" src="./star-store.svg" alt="" />
              <p>Favorites Page</p>
            </div>
          </Link>
          <Link className="navLink" to="/cart">
            <div className="nav-store">
              <img className="buy" src="./buy-store.svg" alt="" />
              <p>Cart Page</p>
            </div>
          </Link>
        </div>
        <div className="store">
          <div className="favorite-store">
            {favorites.length ? (
              <span className="favorite-count">{favorites.length}</span>
            ) : null}
            <img className="star" src="./star-store.svg" alt="" />
          </div>
          <div className="buy-store">
            {cart.length ? (
              <span className="cart-count">{cart.length}</span>
            ) : null}
            <img className="buy" src="./buy-store.svg" alt="" />
          </div>
        </div>
      </header>
    </>
  );
};
Header.defaultProps = {
  favoriteCount: [],
  cartCount: [],
};
export default Header;
