import ProductCard from "../ProductCard/ProductCard";
import "../ProductsList/ProductsList.css";
import { useSelector } from "react-redux";

const ProductsList = (props) => {
  const isLoading = useSelector(state => state.products.isLoading)
  return (
    <>
    {isLoading ? (<div className="lds-dual-ring"></div>) : props.products.map((product) => {
        const { title, price, image, color, vendor } = product;

        return (
          <ProductCard
            key={vendor}
            title={title}
            price={price}
            image={image}
            color={color}
            vendor={vendor}
          />
        );
      })}
      
    </>
  );
};
ProductsList.defaultProps = {
  products: [],
};
export default ProductsList;
