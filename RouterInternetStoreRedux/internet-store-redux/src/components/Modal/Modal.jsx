import "../Modal/Modal.css";
import "../Modal/CartModal.css";
import Buttons from "../Button/Buttons";
import { modalWindowDeclarations } from "./ModalContent";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { hideModal } from "../../store/Modal/modal-actions";
import { addToCart } from "../../store/Cart/cart-actions";
import { removeFromCart } from "../../store/Cart/cart-actions";

const Modal = () => {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);
  const modalContent = getModalContent(modal.renderModal);
  const [renderModal, setRenderModal] = useState(modalContent);

  const modalClose = () => {
    dispatch(hideModal());
  };
  const addCard = () => {
    dispatch(addToCart(modal.card));
    dispatch(hideModal());
  };
  const removeCard = () => {
    dispatch(removeFromCart(modal.card.vendor));
    dispatch(hideModal());
  };

  const { title, price, color, image } = modal.card;
  const { headerText, bodyText, closeButton, modalID, actions } = renderModal;
  const actionsOnClick = [
    { cart: [addCard, modalClose] },
    { delete: [removeCard, modalClose] },
  ];
  const onClickBtn = setOnClickBtn(actionsOnClick, modalID);

  return (
    <>
      <div className="backdrop" onClick={modalClose}></div>
      <div className="modal">
        <div className="modal-header">
          <p>{headerText}</p>
          {closeButton && <Buttons onClick={modalClose} closeStyle={true} />}
        </div>
        <div className="modal-body">
          {modalID === "cart" && (
            <div className="added-item">
              <div className="item-image">
                <img src={image} alt="" />
              </div>
              <p className="item-title">{title}</p>
              <div className="item-description">
                <span className="item-price">{price} UAH</span>
                <span className="item-color">{color}</span>
              </div>
            </div>
          )}
          <p className="bodyText">{bodyText}</p>
        </div>
        <div className="modal-actions">
          <>
            {actions.map((item, index) => {
              const { id, backgroundColor, text } = item;
              return (
                <Buttons
                  key={id}
                  backgroundColor={backgroundColor}
                  text={text}
                  onClick={onClickBtn[index]}
                ></Buttons>
              );
            })}
          </>
        </div>
      </div>
    </>
  );
};
Modal.defaultProps = {
  renderModal: null,
  modalCard: {},
  headerText: "Default header text",
  bodyText: "Default body text",
  actions: [
    {
      id: 1,
      backgroundColor: "#2F88FF",
      text: "Button",
    },
  ],
};
const setOnClickBtn = (array, modalID) => {
  const findActions = array.find((item) => item.hasOwnProperty(modalID));
  return findActions[modalID];
};
const getModalContent = (id) => {
  return modalWindowDeclarations.find((modal) => modal.modalID === id);
};
export default Modal;
