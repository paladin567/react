const getProducts =  () => {
    return fetch('./storeProducts.json', {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => response.json())
    
}

export default getProducts