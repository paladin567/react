import { useEffect } from "react";
import Header from "../components/Header/Header";
import Modal from "../components/Modal/Modal";
import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home/Home";
import Store from "../pages/Store/Store";
import Favorites from "../pages/Favorites/Favorites";
import Error from "../pages/Error/Error";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchProducts } from "../store/Products/products-actions";
import { getDataLS } from "../api/getDataLS"
import { getFavoriteLS } from "../store/Favorites/favorites-action";
import { getCartLS } from "../store/Cart/cart-actions";

const Router = () => {
  const modal = useSelector(state => state.modal);
  const hasError = useSelector(state => state.loadingError)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts())
    const favoriteData = getDataLS("favorite")
    dispatch(getFavoriteLS(favoriteData))
    const cartData = getDataLS("cart")
    dispatch(getCartLS(cartData))
  }, []);

  return (
    <>
      <div className="container">
        <Header />
        <div className="wrapper">
          <Routes>
            <Route path="/" element={hasError ? (<p className="error-msg">Error! Sorry, something wrong...</p>) : (<Home />)} />
            <Route path="/cart" element={<Store />} />
            <Route path="/favorites" element={<Favorites />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </div>
      </div>
      {modal.isOpen && <Modal />}
    </>
  );
};

export default Router;
