import "../Modal/Modal.css";
import "../Modal/CartModal.css";
import Buttons from "../Button/Buttons";
import { modalWindowDeclarations } from "./ModalContent";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { hideModal } from "../../store/Modal/modal-actions";
import {
  addToCart,
  removeFromCart,
  removeAllFromCart,
} from "../../store/Cart/cart-actions";
import ModalCart from "./ModalCart";
import OrderForm from "../Form/Form";

const Modal = () => {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);
  const modalContent = getModalContent(modal.renderModal);
  const [renderModal, setRenderModal] = useState(modalContent);

  const modalClose = () => {
    dispatch(hideModal());
  };
  const addCard = () => {
    dispatch(addToCart(modal.card));
    dispatch(hideModal());
  };
  const removeCard = () => {
    dispatch(removeFromCart(modal.card.vendor));
    dispatch(hideModal());
  };
  const makeOrder = (values, ordered) => {
    let itemsOutput = "";
    const { username, usersurname, userage, address, mobilephone } = values;
    itemsOutput += `Name: ${username}\nLast name: ${usersurname}\nAge:${userage}\nAddress: ${address}\nMobile phone: ${mobilephone}\n\n`;
    ordered.forEach((item) => {
      const { title, color, price, qty } = item;
      itemsOutput += `Title: ${title}\nColor: ${color}\nPrice: ${price}\nQuantity: ${qty}\n\n`;
    });
    console.log(itemsOutput);
    dispatch(removeAllFromCart());
    dispatch(hideModal());
  };

  const { headerText, bodyText, closeButton, modalID, actions } = renderModal;
  const actionsOnClick = [
    { cart: [addCard, modalClose] },
    { delete: [removeCard, modalClose] },
    { order: [() => {}, modalClose] },
  ];
  const onClickBtn = setOnClickBtn(actionsOnClick, modalID);

  return (
    <>
      <div className="backdrop" onClick={modalClose}></div>
      <div className="modal">
        <div className="modal-header">
          <p>{headerText}</p>
          {closeButton && <Buttons onClick={modalClose} closeStyle={true} />}
        </div>
        <div className="modal-body">
          {modalID === "cart" && (
            <ModalCart
              image={modal.card.image}
              title={modal.card.title}
              price={modal.card.price}
              color={modal.card.color}
            />
          )}
          {modalID === "order" && <OrderForm onSubmit={makeOrder} />}
          {modalID !== "order" && <p className="bodyText">{bodyText}</p>}
        </div>
        <div className="modal-actions">
          <>
            {actions.map((item, index) => {
              const { id, backgroundColor, text, type, form } = item;
              return (
                <Buttons
                  key={id}
                  backgroundColor={backgroundColor}
                  text={text}
                  type={type}
                  onClick={onClickBtn[index]}
                  form={form}
                ></Buttons>
              );
            })}
          </>
        </div>
      </div>
    </>
  );
};
Modal.defaultProps = {
  renderModal: null,
  modalCard: {},
  headerText: "Default header text",
  bodyText: "Default body text",
  actions: [
    {
      id: 1,
      backgroundColor: "#2F88FF",
      text: "Button",
    },
  ],
};
const setOnClickBtn = (array, modalID) => {
  const findActions = array.find((item) => item.hasOwnProperty(modalID));
  return findActions[modalID];
};
const getModalContent = (id) => {
  return modalWindowDeclarations.find((modal) => modal.modalID === id);
};
export default Modal;
