const ModalCart = (props) => {
  return (
    <>
      <div className="added-item">
        <div className="item-image">
          <img src={props.image} alt="" />
        </div>
        <p className="item-title">{props.title}</p>
        <div className="item-description">
          <span className="item-price">{props.price} UAH</span>
          <span className="item-color">{props.color}</span>
        </div>
      </div>
    </>
  );
};

export default ModalCart
