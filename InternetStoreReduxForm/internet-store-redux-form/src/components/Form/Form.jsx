import "../Form/Form.css";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { object, string, number } from "yup";
import NumberFormat from "react-number-format";
import { useSelector } from "react-redux";

const CustomErrorMessage = ({ name }) => (
  <ErrorMessage name={name}>
    {(message) => (
      <div className="form-error">
        <i>{message}</i>
      </div>
    )}
  </ErrorMessage>
);

const CustomPhone = ({ field, form: { setFieldValue } }) => (
  <NumberFormat
    name={field.name}
    value={field.mobilephone}
    format="+## (###) ###-####"
    mask="_"
    onBlur={field.onBlur}
    onValueChange={(values) => {
      const { value } = values;
      setFieldValue("mobilephone", value);
    }}
  />
);
const validationSchema = object({
  username: string().required("Name is required"),
  usersurname: string().required("Last name is required"),
  userage: number().typeError("Must be a number").required("Age is required"),
  address: string().required("Address is required"),
  mobilephone: string()
    .required("Mobile is required")
    .min(12, "Enter full phone number"),
});

const OrderForm = (props) => {
  const orderedItems = useSelector((state) => state.cart);

  const handleSubmit = (values) => {
    props.onSubmit(values, orderedItems);
  };

  return (
    <Formik
      initialValues={{
        username: "",
        usersurname: "",
        userage: "",
        address: "",
        mobilephone: "",
      }}
      validateOnChange
      validateOnBlur
      onSubmit={(values, actions) => {
        handleSubmit(values);
      }}
      validationSchema={validationSchema}
    >
      {(props) => (
        <Form id="order" className="order-form">
          <div>
            <label htmlFor="username">Name</label>
            <Field id="username" name="username" />
            <CustomErrorMessage name="username" />
          </div>
          <div>
            <label htmlFor="usersurname">Last Name</label>
            <Field id="usersurname" name="usersurname" />
            <CustomErrorMessage name="usersurname" />
          </div>
          <div>
            <label htmlFor="userage">Age</label>
            <Field id="userage" name="userage" />
            <CustomErrorMessage name="userage" />
          </div>
          <div>
            <label htmlFor="address">Address</label>
            <Field id="address" name="address" />
            <CustomErrorMessage name="address" />
          </div>
          <div>
            <label htmlFor="mobilephone">Mobile Phone</label>
            <Field
              id="mobilephone"
              name="mobilephone"
              component={CustomPhone}
            />
            <CustomErrorMessage name="mobilephone" />
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default OrderForm;
