import "../ProductCard/ProductCard.css";
import Favorite from "../Svg/Favorite";
import Buttons from "../Button/Buttons";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { addFavorite } from "../../store/Favorites/favorites-action";
import { showModalCart } from "../../store/Modal/modal-actions";

const ProductCard = (props) => {
  const dispatch = useDispatch();

  const addToFavorites = () => {
    dispatch(addFavorite(props));
  };

  const modalOpen = () => {
    dispatch(showModalCart({card: props}));
  };
  const { title, price, image, color, vendor} = props;
  const favorites = useSelector((state) => state.favorites);
  const isFavorite = favorites.some((card) => card.vendor === vendor);

  return (
    <>
      <div className="card">
        <div className="add-to-favorite" onClick={addToFavorites}>
          <Favorite fill={isFavorite ? "#FF8000" : "#2F88FF"} />
        </div>
        <div className="image-container">
          <img src={image} alt="" />
        </div>
        <p className="card-title">{title}</p>
        <div className="card-description">
          <span className="price">{price} UAH</span>
          <span className="color">{color}</span>
        </div>
          <Buttons
            backgroundColor="#007bff"
            text="Add to cart"
            onClick={modalOpen}
          />
      </div>
    </>
  );
};

export default ProductCard;
