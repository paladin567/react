import { Component } from "react";
import '../Button/Buttons.css'
class Buttons extends Component {
   handleClick = (e) => {
       this.props.onClick(e)
   }
    render() {
        const {backgroundColor, text, onClick, closeStyle, type, form} = this.props
        return (
          <>
            <button
              className={closeStyle ? ("close-btn") : "btn"}
              style={{ backgroundColor }}
              onClick={onClick && ((e) => this.handleClick(e))}
              type={type}
              form={form}
            >
              {text}
            </button>
          </>
        );
    }
}

export default Buttons