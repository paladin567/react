export const getCartLS = data => {
    return {
        type: 'GET_CART_LOCALSTORAGE',
        payload: { data }
    }
}
export const addToCart = card => {
    return {
        type: 'ADD_TO_CART',
        payload: { card }
    }
}

export const removeFromCart = vendor => {
    return {
        type: 'REMOVE_FROM_CART',
        payload: { vendor }
    }
}

export const removeAllFromCart = () => {
    return {
        type: 'REMOVE_ALL_FROM_CART',
    }
}