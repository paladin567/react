
const INITIAL_STATE = [];
const cart = (state = INITIAL_STATE, action) => {
    switch (action.type) {
		case 'GET_CART_LOCALSTORAGE': {
			return [...action.payload.data]
		}
		case 'ADD_TO_CART': {
			const isExist = state.some((card)=> card.vendor === action.payload.card.vendor);
            if (isExist) return state.map(item => item.vendor === action.payload.card.vendor ? {...item, qty: item.qty + 1} : item)
            return [...state, {...action.payload.card, qty: 1}] 
		}
		case 'REMOVE_FROM_CART': {
			return state.filter((card)=> card.vendor !== action.payload.vendor)
		}
		case 'REMOVE_ALL_FROM_CART': {
			return state.filter((card) => !card.vendor)
		}
		default: {
			return state
		}
	}
}

export default cart