export const loadRequest = () => {
    return {
        type: "LOAD_REQUEST"
    }
}
export const loadSuccess = () => {
    return {
        type: "LOAD_SUCCESS",
    }
}
