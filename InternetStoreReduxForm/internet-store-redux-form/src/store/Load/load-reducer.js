const INITIAL_STATE = {};
const loading = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "LOAD_REQUEST": {
      return true;
    }
    case "LOAD_SUCCESS": {
      return false;
    }
    default: {
      return state;
    }
  }
};

export default loading;
