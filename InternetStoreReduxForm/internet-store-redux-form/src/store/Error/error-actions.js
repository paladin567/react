export const loadedWithError = () => {
    return {
        type: "GET_PRODUCTS_WITH_ERROR"
    }
}
export const loadedWithoutError = () => {
    return {
        type: "GET_PRODUCTS_WITHOUT_ERROR"
    }
}