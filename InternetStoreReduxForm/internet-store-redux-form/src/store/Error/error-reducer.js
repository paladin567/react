const INITIAL_STATE = {};
const loadingError = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "GET_PRODUCTS_WITH_ERROR": {
      return true;
    }
    case "GET_PRODUCTS_WITHOUT_ERROR": {
      return false;
    }
    default: {
      return state;
    }
  }
};

export default loadingError;