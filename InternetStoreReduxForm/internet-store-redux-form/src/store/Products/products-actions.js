import getProducts from "../../api/getProducts";
import { loadRequest, loadSuccess} from "../Load/load-actions";
import { loadedWithError, loadedWithoutError } from "../Error/error-actions";

export const fetchProducts = () => async dispatch => {
    dispatch(loadRequest())
    try {
        const data = await getProducts()
        dispatch(loadSuccess())
        dispatch(loadedWithoutError())
        dispatch(loadedSuccess(data)) 
    } catch (e) {
        dispatch(loadSuccess())
        dispatch(loadedWithError())
    }   
}

export const loadedSuccess = (response) => {
    return {
        type: "GET_PRODUCTS_SUCCESS",
        payload: { response }
    }
}
