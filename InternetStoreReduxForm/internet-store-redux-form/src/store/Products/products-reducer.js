const INITIAL_STATE = [];
const products = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        
        case 'GET_PRODUCTS_SUCCESS': {
            return [...action.payload.response]
        }
        case 'GET_PRODUCTS_ERROR': {
            return [...state]
        }
        default: {
            return state
        }
    }
}

export default products