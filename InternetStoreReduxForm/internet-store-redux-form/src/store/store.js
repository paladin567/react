import {
  legacy_createStore as createStore,
  applyMiddleware,
  compose,
} from "redux";
import rootReducer from "./rootReducer";
import thunk from "redux-thunk";

const INITIAL_STATE = {
};

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : (f) => f;

const syncLS = function (store) {
  return function (next) {
    return function (action) {
      if (action.type === "ADD_FAVORITE") {
        store.getState();
        const result = next(action);
        localStorage.setItem(
          "favorite",
          JSON.stringify(store.getState().favorites)
        );
        return result;
      }
      if (
        action.type === "ADD_TO_CART" ||
        action.type === "REMOVE_FROM_CART" ||
        action.type === "REMOVE_ALL_FROM_CART"
      ) {
        store.getState();
        const result = next(action);
        localStorage.setItem("cart", JSON.stringify(store.getState().cart));
        return result;
      }
      return next(action);
    };
  };
};

const store = createStore(
  rootReducer,
  INITIAL_STATE,
  compose(applyMiddleware(thunk, syncLS), devTools)
);

export default store;
