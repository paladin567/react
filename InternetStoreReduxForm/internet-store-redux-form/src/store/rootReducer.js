import { combineReducers } from "redux";
import favorites from './Favorites/favorites-reducer'
import cart from './Cart/cart-reducer'
import products from "./Products/products-reducer";
import modal from "./Modal/modal-reducer";
import loading from "./Load/load-reducer";
import loadingError from "./Error/error-reducer";
const rootReducer = combineReducers({
    loading,
    loadingError,
    products,
    favorites,
    cart,
    modal
})

export default rootReducer