const INITIAL_STATE = [];
const favorites = (state = INITIAL_STATE, action) => {
    switch (action.type) {
		case 'GET_FAVORITES_LOCALSTORAGE': {
			return [...action.payload.data]
		}
		case 'ADD_FAVORITE': {
			const isExist = state.some((card)=> card.vendor === action.payload.card.vendor)
			if (isExist) return state.filter((card)=> card.vendor !== action.payload.card.vendor);
			return [...state, action.payload.card]
		}
		default: {
			return state
		}
	}
}

export default favorites