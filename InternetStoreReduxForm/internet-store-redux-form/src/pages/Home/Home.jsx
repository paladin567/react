import { useSelector } from "react-redux";
import ProductsList from "../../components/ProductsList/ProductsList";

const Home = () => {
  const products = useSelector((state) => state.products);
  return (
    <>
      <div className="card-wrapper">
        <ProductsList products={products} />
      </div>
    </>
  );
};
Home.defaultProps = {
  products: [],
};
export default Home;
