import ProductsList from "../../components/ProductsList/ProductsList";
import { useSelector } from "react-redux";

const Favorites = () => {
  const favorites = useSelector((state) => state.favorites);

  return (
    <>
    
      {!favorites.length ? (
        <p className="no-items">No items in the favorites</p>
      ) : (
        <div className="card-wrapper">
        <ProductsList products={favorites} />
      </div>
      )}
    </>
  );
};

Favorites.defaultProps = {
  products: [],
  favorite: [],
  addedCart: [],
};
export default Favorites;
