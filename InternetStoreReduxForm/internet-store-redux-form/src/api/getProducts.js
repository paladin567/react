
const getProducts = async () => {
    const response = await fetch('./storeProducts.json')
    const data = await response.json()
    return data   
}

export default getProducts