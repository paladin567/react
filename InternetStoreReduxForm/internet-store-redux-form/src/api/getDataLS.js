export const getDataLS = (category) => {
    const lScategory = localStorage.getItem(category);
    if (!lScategory) return [];
    try {
      const value = JSON.parse(lScategory);
      return value;
    } catch (e) {
      return [];
    }
  };