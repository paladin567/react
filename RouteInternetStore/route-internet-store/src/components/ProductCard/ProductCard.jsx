import "../ProductCard/ProductCard.css";
import Favorite from "../Svg/Favorite";
import Buttons from "../Button/Buttons";

const ProductCard = (props) => {
  const handleClick = () => {
    props.onFavorite();
  };
  const handleModalOpen = () => {
    props.modalOpen();
  };

  const { title, price, image, color, favorite, addedCart } = props;
  return (
    <>
      <div className="card">
        <div className="add-to-favorite" onClick={handleClick}>
          <Favorite fill={favorite ? "#FF8000" : "#2F88FF"} />
        </div>
        <div className="image-container">
          <img src={image} alt="" />
        </div>
        <p className="card-title">{title}</p>
        <div className="card-description">
          <span className="price">{price} UAH</span>
          <span className="color">{color}</span>
        </div>
        {!addedCart ? (
          <Buttons
            backgroundColor="#007bff"
            text="Add to cart"
            onClick={() => handleModalOpen()}
          />
        ) : (
          <Buttons
            backgroundColor="#bebebe"
            text="Added to cart"
            onClick={() => {}}
          />
        )}
      </div>
    </>
  );
};

export default ProductCard;
