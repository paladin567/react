import "../Modal/Modal.css";
import "../Modal/CartModal.css";
import Buttons from "../Button/Buttons";
import { modalWindowDeclarations } from "./ModalContent";
import { useState } from "react";

const Modal = (props) => {
  const modalContent = getModalContent(props.renderModal);
  const [renderModal, setRenderModal] = useState(modalContent);

  const handleClose = () => {
    props.onClose();
  };

  const { title, price, color, image } = props.modalCard;
  const { headerText, bodyText, closeButton, actions, modalID } = renderModal;
  const onClickBtn = setOnClickBtn(props.actionsClick, modalID);

  return (
    <>
      <div className="backdrop" onClick={() => handleClose()}></div>
      <div className="modal">
        <div className="modal-header">
          <p>{headerText}</p>
          {closeButton && (
            <Buttons onClick={() => handleClose()} closeStyle={true} />
          )}
        </div>
        <div className="modal-body">
          {props.renderModal === "cart" && (
            <div className="added-item">
              <div className="item-image">
                <img src={image} alt="" />
              </div>
              <p className="item-title">{title}</p>
              <div className="item-description">
                <span className="item-price">{price} UAH</span>
                <span className="item-color">{color}</span>
              </div>
            </div>
          )}
          <p className="bodyText">{bodyText}</p>
        </div>
        <div className="modal-actions">
          <>
            {actions.map((item, index) => {
              const { id, backgroundColor, text } = item;
              return (
                <Buttons
                  key={id}
                  backgroundColor={backgroundColor}
                  text={text}
                  onClick={onClickBtn[index]}
                ></Buttons>
              );
            })}
          </>
        </div>
      </div>
    </>
  );
};
Modal.defaultProps = {
  renderModal: null,
  modalCard: {},
  headerText: "Default header text",
  bodyText: "Default body text",
  actions: [
    {
      id: 1,
      backgroundColor: "#2F88FF",
      text: "Button",
    },
  ],
};
const setOnClickBtn = (array, modalID) => {
  const findActions = array.find((item) => item.hasOwnProperty(modalID));
  return findActions[modalID];
};
const getModalContent = (id) => {
  return modalWindowDeclarations.find((modal) => modal.modalID === id);
};
export default Modal;
