const modalWindowDeclarations = [
  {
    modalID: "cart",
    headerText: "Add to cart",
    closeButton: true,
    bodyText: "Do you want add this item to your cart?",
    actions: [
      {
        id: 1,
        backgroundColor: "#2F88FF",
        text: "Add to cart",
      },
      {
        id: 2,
        backgroundColor: "#FF8000",
        text: "Cancel",
      },
    ],
  },
  {
    modalID: "delete",
    headerText: "Delete item",
    closeButton: true,
    bodyText: "Do you want delete this item from your cart?",
    actions: [
      {
        id: 1,
        backgroundColor: "#2F88FF",
        text: "Delete",
      },
      {
        id: 2,
        backgroundColor: "#FF8000",
        text: "Cancel",
      },
    ],
  },
];
export { modalWindowDeclarations };
