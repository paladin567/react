import ProductCard from "../ProductCard/ProductCard";
import "../ProductsList/ProductsList.css";

const ProductsList = (props) => {
  const handleToggle = (product, renderModal) => {
    props.isOpenedModal(product, renderModal);
  };
  const onFavorite = (product) => {
    props.onFavorite(product)
  }
  const { products, favorite, addedCart } = props;
  
  return (
    <>
      {products.map((product) => {
        const { title, price, image, color, vendor } = product;
        const isFavorite = favorite.some(card => card.vendor === vendor);
        const isAddedCart = addedCart.some(card => card.vendor === vendor);
        
        return (
          <ProductCard
            key={vendor}
            title={title}
            price={price}
            image={image}
            color={color}
            vendor={vendor}
            onFavorite={()=>onFavorite(product)}
            favorite={isFavorite}
            addedCart={isAddedCart}
            modalOpen={() => handleToggle(product, "cart")}
          />
        );
      })}
    </>
  );
};
ProductsList.defaultProps = {
  products: [],
  favorite: [],
  addedCart: [],
  onFavorite: null
};
export default ProductsList;
