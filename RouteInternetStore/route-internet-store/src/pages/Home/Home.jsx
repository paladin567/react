import ProductsList from "../../components/ProductsList/ProductsList"

const Home = (props) => {
    const handleClickFavorite = (product) => {
        props.onFavorite(product)
    }
    const handleToggle = (product, renderModal) => {
        props.isOpenedModal(product, renderModal)
    }
    return (
        <ProductsList
            products={props.products}
            onFavorite={handleClickFavorite}
            favorite={props.favorite}
            isOpenedModal={handleToggle}
            addedCart={props.addedCart}
          />
    )
}
Home.defaultProps = {
    products: [],
    favorite: [],
    addedCart: [],
  };
export default Home



