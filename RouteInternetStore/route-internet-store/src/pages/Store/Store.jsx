import "../../components/ProductCard/ProductCard.css";

import Buttons from "../../components/Button/Buttons";

const Store = (props) => {
    const handleModalOpen = (vendor, renderModal) => {
        props.isOpenedModal(vendor, renderModal);
      };
  return (
    <>
    {props.products.length === 0 && <p className="no-items">No items in the cart</p>}
      {props.products.map((product) => {
        const { title, price, image, color, vendor } = product;
        return (
       
          <div key={vendor} className="card">
            <div className="delete-from-cart">
              <Buttons onClick={() => handleModalOpen(product, "delete")} closeStyle={true} />
            </div>
            <div className="image-container">
              <img src={image} alt="" />
            </div>
            <p className="card-title">{title}</p>
            <div className="card-description">
              <span className="price">{price} UAH</span>
              <span className="color">{color}</span>
            </div>
          </div>
        );
      })}
    </>
  );
};
Store.defaultProps = {
  products: [],
};
export default Store;
