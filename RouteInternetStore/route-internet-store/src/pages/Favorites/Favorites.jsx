import ProductsList from "../../components/ProductsList/ProductsList";

const Favorites = (props) => {
  const handleClickFavorite = (vendor) => {
    props.onFavorite(vendor);
  };
  const handleToggle = (vendor, renderModal) => {
    props.isOpenedModal(vendor, renderModal);
  };
  return (
    <>
      {props.products.length === 0 && (
        <p className="no-items">No items in the favorites</p>
      )}
      <ProductsList
        products={props.products}
        onFavorite={handleClickFavorite}
        favorite={props.favorite}
        addedCart={props.addedCart}
        isOpenedModal={handleToggle}
      />
    </>
  );
};

Favorites.defaultProps = {
  products: [],
  favorite: [],
  addedCart: [],
};
export default Favorites;
