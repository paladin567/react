import { useState, useEffect } from "react";
import getProducts from "../api/getProducts";
import Header from "../components/Header/Header";
import Modal from "../components/Modal/Modal";
import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home/Home";
import Store from "../pages/Store/Store";
import Favorites from "../pages/Favorites/Favorites";
import Error from "../pages/Error/Error";

const Router = () => {
  const [products, setProducts] = useState([]);
  const [visible, setVisible] = useState(false);
  const [activeCard, setActiveCard] = useState(null);
  const [renderModal, setRenderModal] = useState(null);
  const [favoriteCards, setFavoriteCards] = useState([]);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    getProducts().then((product) => {
      setProducts(product);
    });
    setCart(getDataLS("cart"));
    setFavoriteCards(getDataLS("favorite"));
  }, []);

  useEffect(() => {
    localStorage.setItem("favorite", JSON.stringify(favoriteCards));
  }, [favoriteCards]);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  const toggle = (product, renderModal) => {
    setVisible(!visible);
    setActiveCard(product);
    setRenderModal(renderModal);
  };

  const handleClickBtn = (product) => {
    const isExist = favoriteCards.some(
      (card) => card.vendor === product.vendor
    );
    if (!isExist) {
      setFavoriteCards([...favoriteCards, product]);
    } else {
      setFavoriteCards(
        favoriteCards.filter((card) => card.vendor !== product.vendor)
      );
    }
  };

  const handleAddToCart = () => {
    setCart([...cart, activeCard]);
    toggle();
  };

  const handleDelete = () => {
    const temp = JSON.parse(localStorage.getItem("cart"));
    setCart(temp.filter((card) => card.vendor !== activeCard.vendor));
    toggle();
  };

  return (
    <>
      <div className="container">
        <Header favoriteCount={favoriteCards} cartCount={cart} />
        <div className="card-wrapper">
          <Routes>
            <Route
              path="/"
              element={
                <Home
                  products={products}
                  onFavorite={handleClickBtn}
                  favorite={favoriteCards}
                  isOpenedModal={toggle}
                  addedCart={cart}
                />
              }
            />
            <Route
              path="/cart"
              element={<Store products={cart} isOpenedModal={toggle} />}
            />
            <Route
              path="/favorites"
              element={
                <Favorites
                  products={favoriteCards}
                  onFavorite={handleClickBtn}
                  favorite={favoriteCards}
                  addedCart={cart}
                  isOpenedModal={toggle}
                />
              }
            />
            <Route path="*" element={<Error />} />
          </Routes>
        </div>
      </div>
      {visible && (
        <Modal
          renderModal={renderModal}
          modalCard={activeCard}
          onClose={toggle}
          actionsClick={[
            { cart: [handleAddToCart, toggle] },
            { delete: [handleDelete, toggle] },
          ]}
        />
      )}
    </>
  );
};

const getDataLS = (renderModal) => {
  const lScategory = localStorage.getItem(renderModal);
  if (!lScategory) return [];
  try {
    const value = JSON.parse(lScategory);
    return value;
  } catch (e) {
    return [];
  }
};

export default Router;
