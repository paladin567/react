import { Component } from "react";
import './App.css';
import ProductsList from "./components/ProductsList/ProductsList";
import getProducts from "../src/api/getProducts";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";

const getDataLS = (renderModal) => {
  const lScategory = localStorage.getItem(renderModal)
  if (!lScategory) return []
  try {
    const value = JSON.parse(lScategory)
    return value
  } catch (e) {
    return []
  }
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      products: [],
      visible: false,
      isOpenedModalID: null,
      renderModal: null,
      favoriteCards: [],
      cart: []
    };
  }
  componentDidMount() {
    getProducts().then((product) => {
      this.setState({ products: product });
    });
    const cart = getDataLS('cart')
    const isFavorite = getDataLS('favorite');
    this.setState({
      cart: cart,
      favoriteCards: isFavorite
    })
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.favoriteCards !== this.state.favoriteCards) {
      localStorage.setItem(
        "favorite",
        JSON.stringify(this.state.favoriteCards)
      );
    }
    if (prevState.cart !== this.state.cart) {
      localStorage.setItem(
        "cart",
        JSON.stringify(this.state.cart)
      );
    }
  }

  toggle = (vendor, renderModal) => {
    this.setState({
      visible: !this.state.visible,
      isOpenedModalID: vendor,
      renderModal: renderModal
    })
  }

  handleClickBtn = (vendor) => {
    const newCard = this.state.products.find((item) => item.vendor === vendor);
    this.setState({
      favoriteCards: [...this.state.favoriteCards, newCard.vendor],
    });
    if (this.state.favoriteCards.includes(vendor)) {
      const temp = [...this.state.favoriteCards];
      const index = temp.indexOf(vendor);
      if (index !== -1) {
        temp.splice(index, 1);
        this.setState({ favoriteCards: temp });
      }
    }
  };
  handleAddToCart = () => {
    this.setState({
      cart:[...this.state.cart, this.state.isOpenedModalID]
    })
    this.toggle()
  }
  render() {
    const modalCard = this.state.products.find((item) => item.vendor === this.state.isOpenedModalID);
    return (
      <>
        <div className="container">
          <Header favoriteCount={this.state.favoriteCards} cartCount={this.state.cart} />
          <div className="card-wrapper">
            <ProductsList
              products={this.state.products}
              onFavorite={this.handleClickBtn}
              favorite={this.state.favoriteCards}
              isOpenedModal={this.toggle}
            />
          </div>
        </div>
        {this.state.visible && (
          <Modal
            renderModal={this.state.renderModal}
            headerText="Add to cart"
            bodyText="Do you want add this item to your cart?"
            actions={[
              {
                id: 1,
                backgroundColor: "#2F88FF",
                text: "Add to cart",
                click: this.handleAddToCart,
              },
              {
                id: 2,
                backgroundColor: "#FF8000",
                text: "Cancel",
                click: this.toggle,
              },
            ]}
            modalCard={modalCard}
            closeButton={true}
            onClose={this.toggle}
          />
        )}
      </>
    );
  }
}

export default App;
