import { Component } from "react";
import ProductCard from "../ProductCard/ProductCard"
import "../ProductsList/ProductsList.css"


class ProductsList extends Component {
    
    handleToggle = (vendor, renderModal) => {
        this.props.isOpenedModal(vendor, renderModal)
    }

    render () {
        const {products, favorite} = this.props
      return (
        <>
           {products.map((product) => {
                const {title, price, image, color, vendor} = product
                const isFavorite = favorite.includes(vendor)
                return <ProductCard 
                    key={vendor} 
                    title={title} 
                    price={price} 
                    image={image} 
                    color={color} 
                    vendor={vendor} 
                    onFavorite={this.props.onFavorite} 
                    favorite={isFavorite}
                    modalOpen={()=>this.handleToggle(vendor, "cart")}/>       
           })} 
        </>
      )
    }
  }
  ProductsList.defaultProps = {
    products: [],
    favorite: [],
  }
  export default ProductsList;