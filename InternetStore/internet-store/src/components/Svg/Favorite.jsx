import { Component } from "react";

class Favorite extends Component {

    render() {
        const {fill} = this.props

        return (
          <>
            <svg
              width="48"
              height="48"
              viewBox="0 0 48 48"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect width="48" height="48" fill="white" fillOpacity="0.01" />
              <path
                d="M23.9986 5L17.8856 17.4776L4 19.4911L14.0589 29.3251L11.6544 43L23.9986 36.4192L36.3454 43L33.9586 29.3251L44 19.4911L30.1913 17.4776L23.9986 5Z"
                fill={fill}
                stroke="white"
                strokeWidth="1"
                strokeLinejoin="round"
              />
            </svg>
          </>
        );
    }
}
export default Favorite