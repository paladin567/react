import { Component } from "react";
import "../Header/Header.css"
class Header extends Component {
    render() {
      const {favoriteCount, cartCount} = this.props
        return (
          <>
            <header className="header">
              <div className="logo-container">
                <img src="./logo-store.png" alt="" />
              </div>
              <div className="store">
                <div className="favorite-store">
                  {favoriteCount.length !== 0 ? <span className="favorite-count">{favoriteCount.length}</span> : null}
                  <img className="star" src="./star-store.svg" alt="" />
                </div>
                <div className="buy-store">
                  {cartCount.length !== 0 ? <span className="cart-count">{cartCount.length}</span> : null}
                  <img className="buy" src="./buy-store.svg" alt="" />
                </div>
              </div>
            </header>
          </>
        );
    }
}
export default Header