import { Component } from "react";
import "../ProductCard/ProductCard.css"
import Favorite from "../Svg/Favorite";
import Buttons from "../Button/Buttons";

class ProductCard extends Component {
    constructor(props){
      super()
       
    }
    handleClick = (vendor) => {
        this.props.onFavorite(vendor)
    }
    handleModalOpen = () => {
        this.props.modalOpen()
    }
    render () {
        const {title, price, image, color, vendor, favorite} = this.props
      return (
        <>
        <div className="card">
            <div className="add-to-favorite" onClick={()=>this.handleClick(vendor)}>
                <Favorite fill={favorite ? "#FF8000" : "#2F88FF"}/>
            </div>
            <div className="image-container">
                <img src={image} alt=""/>
            </div>
            <p className="card-title">{title}</p>
            <div className="card-description">
                <span className="price">{price} UAH</span>
                <span className="color">{color}</span>
            </div>
            <Buttons backgroundColor="#007bff" text="Add to cart" onClick={()=>this.handleModalOpen()}/>
        </div>
            
        </>
      )
    }
  }
  
  export default ProductCard;