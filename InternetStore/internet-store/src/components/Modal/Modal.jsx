import { Component } from "react";
import '../Modal/Modal.css'
import '../Modal/CartModal.css'
import Buttons from "../Button/Buttons";


class Modal extends Component {
    
    handleClose = () => {
        this.props.onClose()
    }
    render() {
        const {title, price, color, image} = this.props.modalCard;
       
        return (
            <>
            <div className="backdrop" onClick={()=>this.handleClose()}></div>
            <div className="modal">
                    <div className="modal-header">
                        <p>{this.props.headerText}</p>
                        {this.props.closeButton && (<Buttons onClick={()=>this.handleClose()} closeStyle={true}/>)}
                    </div>
                    <div className="modal-body">
                        {this.props.renderModal === "cart" && (
                            <div className="added-item">
                                <div className="item-image">
                                    <img src={image} alt=""/>
                                </div>
                                <p className="item-title">{title}</p>
                                <div className="item-description">
                                    <span className="item-price">{price} UAH</span>
                                    <span className="item-color">{color}</span>
                                </div>
                            </div>
                        )}
                        <p className="bodyText">{this.props.bodyText}</p>
                        
                    </div>
                    <div className="modal-actions">
                        <>
                        {this.props.actions.map( button => {
                            const {id, backgroundColor, text, click} = button;
                            return <Buttons key={id} backgroundColor={backgroundColor} text={text} onClick={click}/>
                        })}
                        </>
                    </div>
                </div>
            
           
            </>
        )
    }
}
Modal.defaultProps = {
    renderModal: null,
    modalCard: {},
    headerText: "Default header text",
    bodyText: "Default body text",
    actions: [{
        id: 1,
        backgroundColor: "#2F88FF",
        text: "Button"
      }]
}
export default Modal